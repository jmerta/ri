<?php


namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Obr
 *
 * @ORM\Table(name="images")
 * @ORM\Entity()
 * @Vich\Uploadable()
 */
class Image {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     * @var string
     */
    private $image;

    /**
     * @var File
     * @Assert\File(maxSize="3M")
     * @Vich\UploadableField(mapping="image_img", fileNameProperty="image")
     */
    private $imageFile;

    /**
     * @var string|null
     * @ORM\Column(name="size", type="string",length=255, nullable=true)
     */
    private $size;

    /**
     * @var string|null
     * @ORM\Column(name="name", type="string",length=255, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     * @ORM\Column(name="price", type="string",length=255, nullable=true)
     */
    private $price;

    /**
     * @var string|null
     * @ORM\Column(name="material", type="string",length=255, nullable=true)
     */
    private $material;

    /**
     * @var string|null
     * @ORM\Column(name="technique", type="string",length=255, nullable=true)
     */
    private $technique;

    /**
     * @var string|null
     * @ORM\Column(name="description", type="string",length=1500, nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", length=255)
     */
    private $updatedAt;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @param File|null $image
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;

    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    public function __toString()
    {
        return (string)$this->image;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): ?\DateTime {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt(\DateTime $updatedAt): void {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return string|null
     */
    public function getSize(): ?string {
        return $this->size;
    }

    /**
     * @param string|null $size
     */
    public function setSize(?string $size): void {
        $this->size = $size;
    }

    /**
     * @return string|null
     */
    public function getMaterial(): ?string {
        return $this->material;
    }

    /**
     * @param string|null $material
     */
    public function setMaterial(?string $material): void {
        $this->material = $material;
    }

    /**
     * @return string|null
     */
    public function getTechnique(): ?string {
        return $this->technique;
    }

    /**
     * @param string|null $technique
     */
    public function setTechnique(?string $technique): void {
        $this->technique = $technique;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getPrice(): ?string {
        return $this->price;
    }

    /**
     * @param string|null $price
     */
    public function setPrice(?string $price): void {
        $this->price = $price;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void {
        $this->description = $description;
    }

}
