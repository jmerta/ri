<?php
/**
 * Created by PhpStorm.
 * User: janbadura
 * Date: 09/05/2019
 * Time: 13:21
 */

namespace App\Controller;


use App\Entity\Image;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */

    public function index()
    {
        $remainingPaintings = 0;

        $repo = $this->getDoctrine()->getRepository(Image::class);

        /**@var Image[] $images*/
        $images = $repo->findAll();

        return $this->render(
            'homepage/index.html.twig',
            [
                'controllerName' => 'Home',
                'remainingPaintings' => $remainingPaintings,
                'paintings' => $images
            ]
        );
    }
}
